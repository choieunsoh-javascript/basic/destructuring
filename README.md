# Never use the Dot operator in JavaScript, for Data

https://realworldjs.medium.com/never-use-the-dot-operator-in-javascript-ee0339d85fb

- Every time you type a dot . — stop and think about if this is data that you’re operating on. If so — use destructuring instead.
- Destructure as early as possible.
- Remember to include default values, especially in nested destructuring.
