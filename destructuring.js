// Before Destructuring
const getTemperature1 = (atticData) => atticData.celsius || 15;

// * Optional chaining: ?.
// * Nullish coalescing: ??
const getTemperature2 = (atticData) => atticData?.celsius ?? 15;

// * Destructuring
const getTemperature3 = ({ celsius = 15 } = {}) => celsius;

// Chaining
const profile = {
  user: {
    address: {
      street: {
        name: "Krung Thon Buri",
      },
      city: "Khlong San",
    },
  },
};
console.log(profile.user.address.street.name);
console.log(profile.user.address.city);

const street1 = `${profile?.user?.address?.street?.name ?? "Unknown street"}, ${
  profile?.user?.address?.city ?? "Unknown city"
}`;
console.log(street1);

// Destructuring
const {
  user: {
    address: {
      street: { name: streetName },
      city,
    },
  },
} = profile;
console.log(`StreetName = ${streetName}, City = ${city}`);

// Example
const func = (
  {
    fallbackMessage: msg = "Unknown",
    offset = 4,
    pos: { x = 0, y = 0 } = {},
    ...options
  } = {},
  data = {}
) => {
  const { title = "", items: [head, second, ...tail] = [] } = data;
  // some nice logic
  console.log(title);
  console.log(head, second, tail);
};
const data1 = { title: "test", items: [1, 2, 3, 4, 5] };
func({}, data1);
